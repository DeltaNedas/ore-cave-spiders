# Background
I started this mod as a replacement for the old 1.7.10 mod [Ore Spiders](https://www.curseforge.com/minecraft/mc-mods/ore-spiders).
The dev and artist lost communication and the mod was left in the dust.
Then I come in, bored and wanting to bring back some of the good old mods to modern versions of Minecraft.

# Mechanics
Ore Cave Spiders spawn in caves where their ores would spawn
When killed **by a player** they drop a *<material colour>* Cave Spider Egg.
If you place and break an egg you get the material.
You can eat a spider egg for some hunger restoration, nausea and the spider's debuff.

You can combine 8 Overworld eggs around a cobweb into a Spider Nest.
They also spawn rarely in mineshafts.
It summons a Queen Cave Spider and many random spiders that have greatly reduced odds of dropping their material.
A Queen Cave Spider will cycle between random abilities of each spider.

When you kill a queen cave spider it drops a Cobweb Wand.
A **Cobweb Wand** lets you shoot cobwebs consuming 1 string per shot.

Cave spiders spawn in comparable amounts to the ore with exceptions:
- Coal
- Obsidian
- Ender, They only spawn on the outer islands
Some Spiders have **Hardening**. This lets them take half or no damage for a short period of time while remaining still after an attack.
Look below for more info.

# Spawning

Material - Spawn Area - Spawn Dimension - Ability - Poison Type - HP - Speed - Damage - Weakness
Blaze - Nether fortress - Nether - Shoots fiery web - 10s Fire - 15 - 0.75x - 4 + Fire - Water (Splash bottle: hand grenade)
Coal - 1 to 50 - Overworld - Less Knockback - 5s Glowing - 5 - 1x - 4 - Fire, Rapidly takes damage but drops no egg
Diamond - 4 to 16 - Overworld - Half Hardening - 10s Slowness 2 - 15 - 1.5x - 6
Emerald - 4 to 32 - Mountains - Extra Knockback - 5s Poison 2 - 10 - 1.25x - 3 - Runs away from villagers
Ender - Anywhere - Outer end islands - Teleports - 25% chance to teleport player - 10 - 1.5x (Teleportation not counted) - 6 - Water will damage
Gold - 1 to 40 - Nether/Overworld - Jump Boost - Normal poison - 10 - 3x - 2
Iron - 1 to 50 - Overworld - Half Hardening - 5s Slowness - 10 - 0.75x - 6 - Magnets
Lapis - 1 to 31 - Overworld - Extra XP, 10s Levitation - 5 - 1x - 3 - Water to prevent damage, a stone sword as it will 1 shot.
Obsidian - 1 to 10 - Lava pools in Nether/Overworld - No Knockback, Fireproof - 5s Fire - 20 - 0.5x - 5 - Shield: Fire is not given if shield is up, Water: prevents them from exiting lava pool
Quartz - Anywhere - Nether - Fireproof, Extra XP, 10s Weakness - 15 - 1x
Redstone - 1 to 16 - Overworld - Jump Boost - Speed 4 - 10 - 2x - 4 - Knockback is twice as effective as they are so small