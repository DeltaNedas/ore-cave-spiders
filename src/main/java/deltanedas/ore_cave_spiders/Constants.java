package deltanedas.ore_cave_spiders;

public class Constants {
	public static final String MOD_ID = "ore_cave_spiders";
	public static final String MOD_NAME = "Ore Cave Spiders";
	public static final String MOD_VERSION = "__MOD_VERSION";
	public static final String MC_VERSIONS = "[1.12.2]";
	public static final String CLIENT_PROXY = "deltanedas.ore_cave_spiders.proxies.ClientProxy";
	public static final String COMMON_PROXY = "deltanedas.ore_cave_spiders.proxies.CommonProxy";
}
