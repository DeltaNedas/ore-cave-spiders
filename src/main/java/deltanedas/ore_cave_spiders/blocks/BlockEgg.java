package deltanedas.ore_cave_spiders.blocks;

import java.util.List;

import javax.annotation.Nonnull;

import deltanedas.ore_cave_spiders.init.InitConfig;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockEgg extends BaseBlock {
	public static final PropertyDirection FACING = PropertyDirection.create("facing");
	private static AxisAlignedBB[] boundingBox = new AxisAlignedBB[6];

	public BlockEgg(String name, float lightLevel) {
		super(name + "_egg", Material.DRAGON_EGG, lightLevel);
		blockHardness = 5.0F;
		blockResistance = 15.0F;
		setHarvestLevel("Pickaxe", 1);
		setDefaultState(blockState.getBaseState().withProperty(FACING, EnumFacing.UP));
		boundingBox[0] = new AxisAlignedBB(0.25f, 0f, 0.25f, 0.75f, 0.75f, 0.75f); // Up
		boundingBox[1] = new AxisAlignedBB(0.25f, 0.25f, 0.25f, 0.75f, 0.5f, 0.75f); // Down
		boundingBox[2] = new AxisAlignedBB(0.5f, 0.25f, 0.25f, 0.25f, 0.75f, 0.75f); // North
		boundingBox[3] = new AxisAlignedBB(0f, 0.25f, 0.25f, 0.25f, 0.75f, 0.75f); // East
		boundingBox[4] = new AxisAlignedBB(0.25f, 0.25f, 0f, 0.75f, 0.75f, 0.75f); // South
		boundingBox[5] = new AxisAlignedBB(0f, 0.25f, 0.25f, 0.75f, 0.75f, 0.5f); // West
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		tooltip.add(I18n.format("tooltip.spider_egg.1"));
		if (InitConfig.eggsSmeltable) {
			tooltip.add(I18n.format("tooltip.spider_egg.2"));
		}
	}

	@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, FACING);
	}

	private boolean canPlaceOn(World worldIn, BlockPos pos) {
		IBlockState state = worldIn.getBlockState(pos);
		return state.getBlock().canPlaceTorchOnTop(state, worldIn, pos);
	}

	public boolean canPlaceBlockAt(World worldIn, BlockPos pos) {
		for (EnumFacing enumfacing : FACING.getAllowedValues()) {
			if (canPlaceAt(worldIn, pos, enumfacing)) {
				return true;
			}
		}

		return false;
	}

	private boolean canPlaceAt(World worldIn, BlockPos pos, EnumFacing facing) {
		BlockPos blockpos = pos.offset(facing.getOpposite());
		IBlockState iblockstate = worldIn.getBlockState(blockpos);
		Block block = iblockstate.getBlock();
		BlockFaceShape blockfaceshape = iblockstate.getBlockFaceShape(worldIn, blockpos, facing);

		if (facing.equals(EnumFacing.UP) && canPlaceOn(worldIn, blockpos)) {
			return true;
		} else if (facing != EnumFacing.UP) {
			return !isExceptBlockForAttachWithPiston(block) && blockfaceshape == BlockFaceShape.SOLID;
		} else {
			return false;
		}
	}

	@Override
	public IBlockState getStateForPlacement(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer) {
		if (canPlaceAt(worldIn, pos, facing)) {
			return getDefaultState().withProperty(FACING, facing);
		} else {
			for (EnumFacing enumfacing : FACING.getAllowedValues()) {
				if (canPlaceAt(worldIn, pos, enumfacing)) {
					return getDefaultState().withProperty(FACING, enumfacing);
				}
			}

			return getDefaultState();
		}
	}

	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
		checkForDrop(worldIn, pos, state);
	}

	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
		this.onNeighborChangeInternal(worldIn, pos, state);
	}

	protected boolean onNeighborChangeInternal(World worldIn, BlockPos pos, IBlockState state) {
		if (!this.checkForDrop(worldIn, pos, state)) {
			return true;
		} else {
			EnumFacing enumfacing = (EnumFacing)state.getValue(FACING);
			EnumFacing.Axis enumfacing$axis = enumfacing.getAxis();
			EnumFacing enumfacing1 = enumfacing.getOpposite();
			BlockPos blockpos = pos.offset(enumfacing1);
			boolean flag = false;

			if (enumfacing$axis.isHorizontal() && worldIn.getBlockState(blockpos).getBlockFaceShape(worldIn, blockpos, enumfacing) != BlockFaceShape.SOLID) {
				flag = true;
			} else if (enumfacing$axis.isVertical() && !this.canPlaceOn(worldIn, blockpos)) {
				flag = true;
			}

			if (flag) {
				this.dropBlockAsItem(worldIn, pos, state, 0);
				worldIn.setBlockToAir(pos);
				return true;
			} else {
				return false;
			}
		}
	}

	protected boolean checkForDrop(World worldIn, BlockPos pos, IBlockState state) {
		if (state.getBlock() == this && canPlaceAt(worldIn, pos, (EnumFacing) state.getValue(FACING))) {
			return true;
		} else {
			if (worldIn.getBlockState(pos).getBlock() == this) {
				dropBlockAsItem(worldIn, pos, state, 0);
				worldIn.setBlockToAir(pos);
			}

			return false;
		}
	}

	@Override
	public IBlockState getStateFromMeta(int meta) {
		EnumFacing dir = EnumFacing.UP;
		switch(meta) {
		case 1:
			dir = EnumFacing.DOWN; break;
		case 2:
			dir = EnumFacing.NORTH; break;
		case 3:
			dir = EnumFacing.EAST; break;
		case 4:
			dir = EnumFacing.SOUTH; break;
		case 5:
			dir = EnumFacing.WEST; break;
		}
		return getDefaultState().withProperty(FACING, dir);
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	public int getMetaFromState(IBlockState state) {
		int meta = 0;
		switch(state.getValue(FACING)) {
		case DOWN:
			meta = 1; break;
		case NORTH:
			meta = 2; break;
		case EAST:
			meta = 3; break;
		case SOUTH:
			meta = 4; break;
		case WEST:
			meta = 5; break;
		}
		return meta;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public BlockRenderLayer getBlockLayer() {
			return BlockRenderLayer.CUTOUT;
	}

	@Override
	public boolean isOpaqueCube(@Nonnull IBlockState bs) {
		return false;
	}

	@Override
	public boolean isFullCube(@Nonnull IBlockState bs) {
		return false;
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
		return boundingBox[this.getMetaFromState(state)];
	}

	public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
		return BlockFaceShape.UNDEFINED;
	}
}
