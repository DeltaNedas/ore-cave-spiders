package deltanedas.ore_cave_spiders.blocks;

import java.util.Random;

import deltanedas.ore_cave_spiders.init.InitBlocks;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockRedstoneEgg extends BlockEgg {
	public BlockRedstoneEgg() {
		super("redstone", 0F);
	}

	private void activate() {
		setLightLevel(7 / 15);
	}

	private void deactivate() {
		setLightLevel(0);
	}

	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
		if (this == InitBlocks.litRedstoneEgg) {
			deactivate();
		}
	}

	public int tickRate(World worldIn) {
		return 30; // After 1.5 seconds turn from lit to normal
	}

	public void onBlockClicked(World worldIn, BlockPos pos, EntityPlayer playerIn) {
		activate();
		super.onBlockClicked(worldIn, pos, playerIn);
	}

	public void onEntityWalk(World worldIn, BlockPos pos, Entity entityIn) {
		activate();
		super.onEntityWalk(worldIn, pos, entityIn);
	}
}
