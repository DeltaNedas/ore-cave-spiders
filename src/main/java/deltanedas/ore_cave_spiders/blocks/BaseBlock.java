package deltanedas.ore_cave_spiders.blocks;

import deltanedas.ore_cave_spiders.BaseModel;
import deltanedas.ore_cave_spiders.OreCaveSpiders;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

public class BaseBlock extends Block implements BaseModel {
	public BaseBlock(String name, Material mat, float light) {
		super(mat);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(OreCaveSpiders.modTab);
		setLightLevel(light);
		blockHardness = 5.0F;
		blockResistance = 15.0F;
		setHarvestLevel("Pickaxe", 3);

		OreCaveSpiders.BLOCKS.add(this);
		OreCaveSpiders.ITEMS.add(new ItemBlock(this).setRegistryName(this.getRegistryName()));
	}

	@Override
	public void registerModels() {
		OreCaveSpiders.proxy.registerItemRender(Item.getItemFromBlock(this), 0);
	}
}
