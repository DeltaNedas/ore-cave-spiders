package deltanedas.ore_cave_spiders.blocks;

import java.util.List;

import javax.annotation.Nonnull;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockSpiderNest extends BaseBlock {
	protected static AxisAlignedBB boundingBox = new AxisAlignedBB(0f, 0f, 0f, 0.75f, 0.75f, 0.75f);
	public BlockSpiderNest() {
		super("spider_nest", Material.DRAGON_EGG, 0F);
		blockHardness = 5.0F;
		blockResistance = 15.0F;
		setHarvestLevel("Pickaxe", 1);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		tooltip.add(I18n.format("tooltip.spider_nest.1"));
		tooltip.add(I18n.format("tooltip.spider_nest.2"));
	}

	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
		if (worldIn.getDifficulty() != EnumDifficulty.PEACEFUL) {
			// Summon queen
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public BlockRenderLayer getBlockLayer() {
			return BlockRenderLayer.CUTOUT;
	}

	@Override
	public boolean isOpaqueCube(@Nonnull IBlockState bs) {
		return false;
	}

	@Override
	public boolean isFullCube(@Nonnull IBlockState bs) {
		return false;
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
		return boundingBox;
	}

	public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
		return BlockFaceShape.UNDEFINED;
	}
}
