package deltanedas.ore_cave_spiders;


import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

public class Smelting {
	public static void add(ItemStack input, ItemStack output, float xp) {
		FurnaceRecipes.instance().addSmeltingRecipe(input, output, xp);
	}
	
	public static void add(Item input, int inputCount, Item output, int outputCount, float xp) {
		add(
			new ItemStack(
				input,
				inputCount),
			new ItemStack(
				output,
				outputCount),
			xp);
	}

	public static void add(Block input, int inputCount, Item output, int outputCount, float xp) {
		add(
			new ItemStack(
				input,
				inputCount),
			new ItemStack(
				output,
				outputCount),
			xp);
	}
}