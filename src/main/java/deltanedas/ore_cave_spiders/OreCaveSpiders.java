package deltanedas.ore_cave_spiders;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import deltanedas.ore_cave_spiders.init.InitBlocks;
import deltanedas.ore_cave_spiders.init.InitConfig;
import deltanedas.ore_cave_spiders.init.InitEntities;
import deltanedas.ore_cave_spiders.init.InitItems;
import deltanedas.ore_cave_spiders.init.InitRecipes;
import deltanedas.ore_cave_spiders.items.CobwebWand;
import deltanedas.ore_cave_spiders.proxies.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(
	modid = Constants.MOD_ID,
	name = Constants.MOD_NAME,
	version = Constants.MOD_VERSION,
	acceptedMinecraftVersions = Constants.MC_VERSIONS
)
public class OreCaveSpiders {
	public static final List<Block> BLOCKS = new ArrayList<Block>();
	public static final List<Entity> ENTITIES = new ArrayList<Entity>();
	public static final List<Item> ITEMS = new ArrayList<Item>();

	public static Item cobwebWand = new CobwebWand();

	public static final CreativeTabs modTab = (new CreativeTabs("tabOreCaveSpiders") {
		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(cobwebWand);
		}
	});

	@Instance
	public static OreCaveSpiders instance;

	@SidedProxy(
		clientSide = Constants.CLIENT_PROXY,
		serverSide = Constants.COMMON_PROXY
	)
	public static CommonProxy proxy;
	public static Logger logger;
	public static Level level = Level.INFO;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger = event.getModLog();
		cobwebWand.setCreativeTab(modTab);
		new InitConfig(); // Does config stuff
		new InitItems(); // Creates the Items
		new InitBlocks(); // Creates the eggs and magnet
		new InitEntities(); // Creates the spiders!
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		new InitRecipes(); // Fried eggs
	}

	public static void log(String text) {
		if (logger != null) {
			logger.log(level, text);
		}
	}
}
