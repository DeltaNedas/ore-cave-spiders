package deltanedas.ore_cave_spiders.ai;

import deltanedas.ore_cave_spiders.entities.EntityWebShot;
import deltanedas.ore_cave_spiders.entities.spiders.EntityOreCaveSpider;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.util.math.BlockPos;
// import net.minecraft.util.math.MathHelper;

public class AIWebShoot extends EntityAIBase {
	private final EntityOreCaveSpider spider;
	private int attackStep;
	private int attackTime;
	private Potion effectType;
	private double effectTime;
	private int fireTime;
	
	public AIWebShoot(EntityOreCaveSpider spiderIn, Potion effectType, double effectTime, int fireTime) {
		this.spider = spiderIn;
		this.setMutexBits(3);
		this.effectType = effectType;
		this.effectTime = effectTime;
		this.fireTime = fireTime;
	}
	
	/**
	 * Returns whether the EntityAIBase should begin execution.
	 */
	public boolean shouldExecute() {
		EntityLivingBase entitylivingbase = this.spider.getAttackTarget();
		return entitylivingbase != null && entitylivingbase.isEntityAlive();
	}
	
	/**
	 * Execute a one shot task or start executing a continuous task
	 */
	public void startExecuting() {
		this.attackStep = 0;
	}
	
	/**
	 * Reset the task's internal state. Called when this task is interrupted by another one
	 */
	public void resetTask() {
		spider.setAIMoveSpeed((float) spider.speed); // Only charge up when firing
	}
	
	/**
	 * Keep ticking a continuous task that has already been started
	 */
	public void updateTask() {
		--this.attackTime;
		EntityLivingBase entitylivingbase = this.spider.getAttackTarget();
		double d0 = this.spider.getDistanceSq(entitylivingbase);
		
		if (d0 < 4.0D) {
			if (this.attackTime <= 0) {
				this.attackTime = 20;
				this.spider.attackEntityAsMob(entitylivingbase);
			}
			
			this.spider.getMoveHelper().setMoveTo(entitylivingbase.posX, entitylivingbase.posY, entitylivingbase.posZ, 1.0D);
		} else if (d0 < this.getFollowDistance() * this.getFollowDistance()) {
			// double d1 = entitylivingbase.posX - this.spider.posX;
			// double d2 = entitylivingbase.getEntityBoundingBox().minY + (double)(entitylivingbase.height / 2.0F) - (this.spider.posY + (double)(this.spider.height / 2.0F));
			// double d3 = entitylivingbase.posZ - this.spider.posZ;
			
			if (this.attackTime <= 0) {
				++this.attackStep;
				
				if (this.attackStep == 1) {
					this.attackTime = 60; // Wait for 3 seconds then shoot and move
					this.spider.setAIMoveSpeed(0);
				} else if (this.attackStep <= 4) {
					this.attackTime = 6;
				} else {
					this.attackTime = 100;
					this.attackStep = 0;
					this.spider.setAIMoveSpeed((float) spider.speed);
				}
				
				if (this.attackStep > 1) {
					//float f = MathHelper.sqrt(MathHelper.sqrt(d0)) * 0.5F;
					this.spider.world.playEvent((EntityPlayer)null, 1018, new BlockPos((int)this.spider.posX, (int)this.spider.posY, (int)this.spider.posZ), 0);
					
					EntityWebShot.shootWeb(spider.getRNG(), (EntityPlayer) entitylivingbase, spider.world, effectType, effectTime, fireTime);
				}
			}
			
			this.spider.getLookHelper().setLookPositionWithEntity(entitylivingbase, 10.0F, 10.0F);
		} else {
			this.spider.getNavigator().clearPath();
			this.spider.getMoveHelper().setMoveTo(entitylivingbase.posX, entitylivingbase.posY, entitylivingbase.posZ, 1.0D);
		}
		
		super.updateTask();
	}
	
	private double getFollowDistance() {
		IAttributeInstance iattributeinstance = this.spider.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE);
		return iattributeinstance == null ? 16.0D : iattributeinstance.getAttributeValue();
	}
}