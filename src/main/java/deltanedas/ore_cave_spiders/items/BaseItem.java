package deltanedas.ore_cave_spiders.items;

import deltanedas.ore_cave_spiders.BaseModel;
import deltanedas.ore_cave_spiders.OreCaveSpiders;
import net.minecraft.item.Item;

public class BaseItem extends Item implements BaseModel {
	public BaseItem(String name) {
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(OreCaveSpiders.modTab);

		OreCaveSpiders.ITEMS.add(this);
	}

	@Override
	public void registerModels() {
		OreCaveSpiders.proxy.registerItemRender(this, 0);
	}
}
