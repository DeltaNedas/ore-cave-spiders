package deltanedas.ore_cave_spiders.items;

import java.util.List;

import deltanedas.ore_cave_spiders.Util;
import deltanedas.ore_cave_spiders.entities.EntityWebShot;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class CobwebWand extends BaseItem {
	public CobwebWand() {
		super("cobweb_wand");
		maxStackSize = 1;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
		tooltip.add(I18n.format("tooltip.cobweb_wand.1"));
		tooltip.add(I18n.format("tooltip.cobweb_wand.2"));
	}

	public boolean onItemUse(ItemStack itemstack, EntityPlayer playerIn, World worldIn) {
		ItemStack string = Util.playerContainsItem(playerIn.inventory, "string");
		if (string != null) {
			if (!playerIn.isCreative()) {
				string.shrink(1);
			}
			EntityWebShot.shootWeb(itemRand, playerIn, worldIn, MobEffects.SLOWNESS, 7.5, 0);
			return true;
		}
		return false;
	}
}