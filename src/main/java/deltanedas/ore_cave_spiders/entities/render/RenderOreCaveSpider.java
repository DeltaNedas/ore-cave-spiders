package deltanedas.ore_cave_spiders.entities.render;

import deltanedas.ore_cave_spiders.Constants;
import deltanedas.ore_cave_spiders.entities.spiders.EntityOreCaveSpider;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderSpider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderOreCaveSpider extends RenderSpider<EntityOreCaveSpider> {
	private ResourceLocation textures;

	public RenderOreCaveSpider(RenderManager renderManagerIn, String name) {
		super(renderManagerIn);
		textures = new ResourceLocation(Constants.MOD_ID + ":textures/entities/" + name + ".png");
		this.shadowSize *= 0.7F;
	}

	/**
	 * Allows the render to do state modifications necessary before the model is rendered.
	 */
	protected void preRenderCallback(EntityOreCaveSpider entitylivingbaseIn, float partialTickTime) {
		GlStateManager.scale(0.7F, 0.7F, 0.7F);
	}

	/**
	 * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
	 */
	protected ResourceLocation getEntityTexture(EntityOreCaveSpider entity) {
		return textures;
	}
}