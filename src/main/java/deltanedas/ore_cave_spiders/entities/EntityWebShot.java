package deltanedas.ore_cave_spiders.entities;

import java.util.Random;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;

public class EntityWebShot extends EntityThrowable {
	EntityLiving thrower;
	Potion effectType = MobEffects.SLOWNESS;
	double effectTime = 7;
	int fireTime = 0;

	public EntityWebShot(World worldIn) {
		super(worldIn);
	}

	public EntityWebShot(World worldIn, EntityLivingBase throwerIn, Potion effectType, double effectTime, int fireTime) {
		super(worldIn, throwerIn);
		thrower = (EntityLiving) throwerIn;
		this.effectType = effectType;
		this.effectTime = effectTime;
		this.fireTime = fireTime;
	}

	public EntityWebShot(World worldIn, EntityLivingBase throwerIn) {
		super(worldIn, throwerIn);
		thrower = (EntityLiving) throwerIn;
	}

	public EntityWebShot(World worldIn, double x, double y, double z) {
		super(worldIn, x, y, z);
	}

	public static void shootWeb(Random itemRand, EntityPlayer playerIn, World worldIn, Potion effectType, double effectTime, int fireTime) {
		worldIn.playSound((EntityPlayer) null, playerIn.posX, playerIn.posY, playerIn.posZ, SoundEvents.ENTITY_SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F)); // Same sound as a snowball
		if (!worldIn.isRemote) {
			EntityWebShot shot = new EntityWebShot(worldIn, playerIn, effectType, effectTime, fireTime);
			shot.shoot(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, 0.0F, 2.0F, 1.0F); // 33% faster than a snowball
			worldIn.spawnEntity(shot);
		}
	}

	public void applyEffect(RayTraceResult result) {
		if (result.entityHit != thrower && !(result.entityHit instanceof EntitySpider)) { // For not you or spiders
			if (effectType != null && effectTime > 0) {
				((EntityLivingBase) result.entityHit).addPotionEffect(new PotionEffect(effectType, (int) (effectTime * 20), 0)); // Apply Slowness I for 5 seconds
			}
			if (fireTime > 0) {
				result.entityHit.setFire(fireTime); // Blaze Cave Spider can do this
			}
		}

		BlockPos pos = result.getBlockPos();
		IBlockState existing = world.getBlockState(pos);
		if (existing == null || existing instanceof IPlantable) { // Plants and air may be replaced
			world.setBlockState(pos, Blocks.WEB.getDefaultState());
		}
	}

	protected void onImpact(RayTraceResult result)  {
		if (result.entityHit != null) { // Apply slowness
			applyEffect(result);
		}

		if (!world.isRemote) {
			world.setEntityState(this, (byte)3);
			setDead();
		}
	}
}