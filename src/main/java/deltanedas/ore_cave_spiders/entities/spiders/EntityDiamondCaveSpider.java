package deltanedas.ore_cave_spiders.entities.spiders;

import deltanedas.ore_cave_spiders.entities.render.RenderOreCaveSpider;
import deltanedas.ore_cave_spiders.init.InitEntities;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class EntityDiamondCaveSpider extends EntityOreCaveSpider {
	double maxHealth = 10;
	double speed = this.speed * 1;
	double attackDamage = 3;
	
	public EntityDiamondCaveSpider(World worldIn) {
		super(worldIn);
	}

	public static void registerEntity() {
		InitEntities.registerEntity("diamond_cave_spider", EntityDiamondCaveSpider.class, id, 30, 13956338, 2154970);
		RenderingRegistry.registerEntityRenderingHandler(EntityDiamondCaveSpider.class, new IRenderFactory<EntityOreCaveSpider>() {
			@Override
			public Render<? super EntityOreCaveSpider> createRenderFor(RenderManager manager) {
				return new RenderOreCaveSpider(manager, "diamond_cave_spider");
			}
		});
		id++;
	}

	@Override
	protected void applyAttackEffect(Entity entityIn) {
		double time = 0;

		if (world.getDifficulty() == EnumDifficulty.NORMAL) {
			time = 7.5;
		} else if (world.getDifficulty() == EnumDifficulty.HARD) {
			time = 15;
		}

		if (time > 0) {
			((EntityLivingBase) entityIn).addPotionEffect(new PotionEffect(MobEffects.POISON, (int) (time * 20), 0));
		}
	}
}