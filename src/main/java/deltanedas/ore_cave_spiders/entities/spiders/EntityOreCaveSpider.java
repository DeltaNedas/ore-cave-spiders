package deltanedas.ore_cave_spiders.entities.spiders;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class EntityOreCaveSpider extends EntitySpider {
	protected static int id = 46780;

	public double maxHealth = 12.0D;
	public double attackDamage = 3.0D;
	public double speed = 0.30000001192092896D;

	public EntityOreCaveSpider(World worldIn) {
		super(worldIn);
		setSize(0.7F, 0.5F);
	}

	public static void registerEntity() {} // An Ore Cave Spider with no Ore is just a weak Ore Cave Spider.

	protected void applyEntityAttributes() {
		super.applyEntityAttributes();
		getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(maxHealth);
		getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(speed);
		getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(attackDamage);
    getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(48.0D);
	}

	protected void applyAttackEffect(Entity entityIn) {
		double time = 0;

		if (world.getDifficulty() == EnumDifficulty.NORMAL) {
			time = 7.5;
		} else if (world.getDifficulty() == EnumDifficulty.HARD) {
			time = 15;
		}

		if (time > 0) {
			((EntityLivingBase) entityIn).addPotionEffect(new PotionEffect(MobEffects.POISON, (int) (time * 20), 0));
		}
	}

	public boolean attackEntityAsMob(Entity entityIn) {
		if (super.attackEntityAsMob(entityIn)) {
			if (entityIn instanceof EntityLivingBase) {
				applyAttackEffect(entityIn);
			}
			return true;
		} else {
			return false;
		}
	}
}