package deltanedas.ore_cave_spiders.entities.spiders;

import deltanedas.ore_cave_spiders.ai.AIWebShoot;
import deltanedas.ore_cave_spiders.entities.render.RenderOreCaveSpider;
import deltanedas.ore_cave_spiders.init.InitEntities;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class EntityBlazeCaveSpider extends EntityOreCaveSpider {
	protected int fireSeconds = 10;
	double maxHealth = 10;
	double speed = this.speed * 1;
	double attackDamage = 3;

	public EntityBlazeCaveSpider(World worldIn) {
		super(worldIn);
    setPathPriority(PathNodeType.WATER, -1.0F); // Same proprities as a blaze
    setPathPriority(PathNodeType.LAVA, 8.0F);
    setPathPriority(PathNodeType.DANGER_FIRE, 0.0F);
    setPathPriority(PathNodeType.DAMAGE_FIRE, 0.0F);
    isImmuneToFire = true;
	}
	
	protected void initEntityAI() {
      this.tasks.addTask(4, new AIWebShoot(this, null, 0, 10));
      this.tasks.addTask(5, new EntityAIMoveTowardsRestriction(this, 1.0D));
      this.tasks.addTask(7, new EntityAIWanderAvoidWater(this, 1.0D, 0.0F));
      this.tasks.addTask(8, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
      this.tasks.addTask(8, new EntityAILookIdle(this));
      this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, true, new Class[0]));
      this.targetTasks.addTask(2, new EntityAINearestAttackableTarget<EntityPlayer>(this, EntityPlayer.class, true));
  }
	
	@Override
	public void onLivingUpdate() {
		if (world.isRemote) { // Make blaze noises
			if (rand.nextInt(24) == 0 && !this.isSilent()) {
				world.playSound(this.posX + 0.5D, this.posY + 0.5D, this.posZ + 0.5D, SoundEvents.ENTITY_BLAZE_BURN, this.getSoundCategory(), 1.0F + this.rand.nextFloat(), this.rand.nextFloat() * 0.7F + 0.3F, false);
			}
		}
		super.onLivingUpdate(); // Also make spider noises
	}

	public static void registerEntity() {
		InitEntities.registerEntity("blaze_cave_spider", EntityBlazeCaveSpider.class, id, 30, 16771587, 14775573);
		RenderingRegistry.registerEntityRenderingHandler(EntityBlazeCaveSpider.class, new IRenderFactory<EntityOreCaveSpider>() {
			@Override
			public Render<? super EntityOreCaveSpider> createRenderFor(RenderManager manager) {
				return new RenderOreCaveSpider(manager, "blaze_cave_spider");
			}
		});
		id++;
	}

	@Override
	protected void applyAttackEffect(Entity entityIn) {
		int time = fireSeconds;

		if (world.getDifficulty() == EnumDifficulty.EASY) {
			time *= 0.75;
		} else if (world.getDifficulty() == EnumDifficulty.HARD) {
			time *= 1.25;
		}

		((EntityLivingBase) entityIn).setFire(time);
	}
}