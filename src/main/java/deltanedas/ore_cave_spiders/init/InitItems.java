package deltanedas.ore_cave_spiders.init;

import deltanedas.ore_cave_spiders.items.CrownJewel;
import net.minecraft.item.Item;

public class InitItems {
	public static Item crownJewel;

	public InitItems() {
		crownJewel = new CrownJewel();
	}
}
