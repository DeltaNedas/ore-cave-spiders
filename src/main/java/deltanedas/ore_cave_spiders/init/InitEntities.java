package deltanedas.ore_cave_spiders.init;

import deltanedas.ore_cave_spiders.Constants;
import deltanedas.ore_cave_spiders.OreCaveSpiders;
import deltanedas.ore_cave_spiders.entities.spiders.EntityBlazeCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityCoalCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityDiamondCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityEmeraldCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityEnderCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityGoldCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityIronCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityLapisCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityObsidianCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityQuartzCaveSpider;
import deltanedas.ore_cave_spiders.entities.spiders.EntityRedstoneCaveSpider;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class InitEntities {
	public InitEntities() {
		EntityBlazeCaveSpider.registerEntity();
		EntityCoalCaveSpider.registerEntity();
		EntityDiamondCaveSpider.registerEntity();
		EntityEmeraldCaveSpider.registerEntity();
		EntityEnderCaveSpider.registerEntity();
		EntityGoldCaveSpider.registerEntity();
		EntityIronCaveSpider.registerEntity();
		EntityLapisCaveSpider.registerEntity();
		EntityObsidianCaveSpider.registerEntity();
		EntityQuartzCaveSpider.registerEntity();
		EntityRedstoneCaveSpider.registerEntity();
		/*coalSpider = new EntityOreCaveSpider("coal", 2894628, 1973529);
		diamondSpider = new EntityOreCaveSpider("diamond", 13956338, 2154970);
		emeraldSpider = new EntityOreCaveSpider("emerald", 5961815, 7985521);
		enderSpider = new EntityOreCaveSpider("ender", 869393, 2894884);
		goldSpider = new EntityOreCaveSpider("gold", 15922249, 14342205);
		ironSpider = new EntityOreCaveSpider("iron", 13619151, 12369084);
		lapisSpider = new EntityOreCaveSpider("lapis", 5324232, 7631045);
		obsidianSpider = new EntityOreCaveSpider("obsidian", 6112600, 197378);
		quartzSpider = new EntityOreCaveSpider("quartz", 16053492, 14145229);
		redstoneSpider = new EntityOreCaveSpider("redstone", 16459302, 12204345);*/
	}

	public static void registerEntity(String name, Class<? extends Entity> entity, int id, int range, int colourPrimary, int colourSecondary) {
		EntityRegistry.registerModEntity(new ResourceLocation(Constants.MOD_ID + ":" + name), entity, name, id, OreCaveSpiders.instance, range, 1, true, colourPrimary, colourSecondary);
	}
}
