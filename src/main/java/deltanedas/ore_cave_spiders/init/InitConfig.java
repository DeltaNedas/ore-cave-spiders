package deltanedas.ore_cave_spiders.init;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.Config.Comment;
import net.minecraftforge.common.config.Config.RequiresMcRestart;

@Config(modid = "ore_cave_spiders")
public class InitConfig {
	// Eggs

	@Comment("If set to false, Cave Spider Eggs are cosmetic blocks only.")
	@RequiresMcRestart
	public static boolean eggsSmeltable = true;

	// Here be spawns

	@Comment("If Blaze Cave Spiders will spawn in Nether fortresses.")
	@RequiresMcRestart
	public static boolean spawnBlazeCaveSpiders = true;

	@Comment("Maximum Y level Blaze Cave Spiders can spawn in.\nDefault is 128.")
	@RequiresMcRestart
	public static int blazeCaveSpiderAltitude = 128; // Nether roof is at 128

	@Comment("Minimum number of Blaze Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int blazeCaveSpiderMin = 1;

	@Comment("Maximum number of Blaze Cave Spiders that will spawn in a 'vein'.\nDefault is 4.")
	public static int blazeCaveSpiderMax = 4;

	@Comment("If Coal Cave Spiders will spawn in the Overworld.")
	@RequiresMcRestart
	public static boolean coalCaveSpiderSpawns = true;

	@Comment("Maximum Y level Coal Cave Spiders can spawn in.\nDefault is 256.")
	@RequiresMcRestart
	public static int coalCaveSpiderAltitude = 70; // Balance

	@Comment("Minimum number of Coal Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int coalCaveSpiderMin = 2;

	@Comment("Maximum number of Coal Cave Spiders that will spawn in a 'vein'.\nDefault is 10.")
	public static int coalCaveSpiderMax = 10;

	@Comment("If Diamond Cave Spiders will spawn in the Overworld.")
	@RequiresMcRestart
	public static boolean diamondCaveSpiderSpawns = true;

	@Comment("Maximum Y level Diamond Cave Spiders can spawn in.\nDefault is 256.")
	@RequiresMcRestart
	public static int diamondCaveSpiderAltitude = 16;

	@Comment("Minimum number of Diamond Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int diamondCaveSpiderMin = 1;

	@Comment("Maximum number of Diamond Cave Spiders that will spawn in a 'vein'.\nDefault is 8.")
	public static int diamondCaveSpiderMax = 8;

	@Comment("If Emerald Cave Spiders will spawn in emerald spawning biomes.")
	@RequiresMcRestart
	public static boolean emeraldCaveSpiderSpawns = true;

	@Comment("Maximum Y level Emerald Cave Spiders can spawn in.\nDefault is 32.")
	@RequiresMcRestart
	public static int emeraldCaveSpiderAltitude = 32;

	@Comment("Minimum number of Emerald Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int emeraldCaveSpiderMin = 1;

	@Comment("Maximum number of Emerald Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int emeraldCaveSpiderMax = 1;

	@Comment("If Cave Enderspiders will spawn in the outer End islands.")
	@RequiresMcRestart
	public static boolean enderCaveSpiderSpawns = true;

	@Comment("Maximum Y level Cave Enderspiders can spawn in.\nDefault is 256.")
	@RequiresMcRestart
	public static int enderCaveSpiderAltitude = 256;

	@Comment("Minimum number of Cave Enderspiders that will spawn in a '''vein'''.\nDefault is 1.")
	public static int enderCaveSpiderMin = 1;

	@Comment("Maximum number of Cave Enderspiders that will spawn in a '''vein'''.\nDefault is 3.")
	public static int enderCaveSpiderMax = 3;

	@Comment("If Gold Cave Spiders will spawn in the Overworld.")
	@RequiresMcRestart
	public static boolean goldCaveSpiderSpawns = true;

	@Comment("Maximum Y level Gold Cave Spiders can spawn in.\nDefault is 32.")
	@RequiresMcRestart
	public static int goldCaveSpiderAltitude = 32;

	@Comment("Minimum number of Gold Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int goldCaveSpiderMin = 1;

	@Comment("Maximum number of Gold Cave Spiders that will spawn in a 'vein'.\nDefault is 10.")
	public static int goldCaveSpiderMax = 10;

	@Comment("If Iron Cave Spiders will spawn in the Overworld.")
	@RequiresMcRestart
	public static boolean ironCaveSpiderSpawns = true;

	@Comment("Maximum Y level Iron Cave Spiders can spawn in.\nDefault is 256.")
	@RequiresMcRestart
	public static int ironCaveSpiderAltitude = 60;

	@Comment("Minimum number of Iron Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int ironCaveSpiderMin = 1;

	@Comment("Maximum number of Iron Cave Spiders that will spawn in a 'vein'.\nDefault is 10.")
	public static int ironCaveSpiderMax = 10;

	@Comment("If Lapis Cave Spiders will spawn in Nether fortresses.")
	@RequiresMcRestart
	public static boolean lapisCaveSpiderSpawns = true;

	@Comment("Maximum Y level Lapis Cave Spiders can spawn in")
	@RequiresMcRestart
	public static int lapisCaveSpiderAltitude = 31;

	@Comment("Minimum number of Lapis Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int lapisCaveSpiderMin = 1;

	@Comment("Maximum number of Lapis Cave Spiders that will spawn in a 'vein'.\nDefault is 8.")
	public static int lapisCaveSpiderMax = 8;

	@Comment("If obsidian Cave Spiders will spawn in lava pools in the Nether and Overworld.")
	@RequiresMcRestart
	public static boolean obsidianCaveSpiderSpawns = true;

	@Comment("Maximum Y level Obsidian Cave Spiders can spawn in.\nDefault is 10")
	@RequiresMcRestart
	public static int obsidianCaveSpiderAltitude = 10;

	@Comment("Minimum number of Obsidian Cave Spiders that will spawn in a 'vein'.\nDefault is 1.")
	public static int obsidianCaveSpiderMin = 1;

	@Comment("Maximum number of Obsidian Cave Spiders that will spawn in a 'vein'.\nDefault is 5.")
	public static int obsidianCaveSpiderMax = 5;

	@Comment("If Quartz Cave Spiders will spawn in the nether.")
	@RequiresMcRestart
	public static boolean quartzCaveSpiderSpawns = true;

	@Comment("Maximum Y level Quartz Cave Spiders can spawn in.\nDefault is 117")
	@RequiresMcRestart
	public static int quartzCaveSpiderAltitude = 117;

	@Comment("Minimum number of Quartz Cave Spiders that will spawn in a 'vein'.\nDefault is 4.")
	public static int quartzCaveSpiderMin = 4;

	@Comment("Maximum number of Quartz Cave Spiders that will spawn in a 'vein'.\nDefault is 10.")
	public static int quartzCaveSpiderMax = 10;

	@Comment("If Redstone Cave Spiders will spawn in the overworld.")
	@RequiresMcRestart
	public static boolean redstoneCaveSpiderSpawns = true;

	@Comment("Maximum Y level Redstone Cave Spiders can spawn in.\nDefault is 16.")
	@RequiresMcRestart
	public static int redstoneCaveSpiderAltitude = 16;

	@Comment("Minimum number of Redstone Cave Spiders that will spawn in a 'vein'.\nDefault is 4.")
	public static int redstoneCaveSpiderMin = 4;

	@Comment("Maximum number of Redstone Cave Spiders that will spawn in a 'vein'.\nDefault is 8.")
	public static int redstoneCaveSpiderMax = 8;
}

