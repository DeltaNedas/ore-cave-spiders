package deltanedas.ore_cave_spiders.init;

import deltanedas.ore_cave_spiders.blocks.BlockEgg;
import deltanedas.ore_cave_spiders.blocks.BlockEnderEgg;
import deltanedas.ore_cave_spiders.blocks.BlockRedstoneEgg;
import deltanedas.ore_cave_spiders.blocks.BlockSpiderNest;
import net.minecraft.block.Block;

public class InitBlocks {
	public static Block blazeEgg;
	public static Block coalEgg;
	public static Block diamondEgg;
	public static Block emeraldEgg;
	public static Block enderEgg; // Not to be confused with Mojang(R)'s Minecraft(TM)'s Ender Dragon Egg!
	public static Block goldEgg;
	public static Block ironEgg;
	public static Block lapisEgg;
	public static Block obsidianEgg;
	public static Block quartzEgg;
	public static Block redstoneEgg;
	public static Block litRedstoneEgg;
	public static Block spiderNest;

	public InitBlocks() {
		blazeEgg = new BlockEgg("blaze", 1F);
		coalEgg = new BlockEgg("coal", 1F / 15F);
		diamondEgg = new BlockEgg("diamond", 3F / 15F);
		emeraldEgg = new BlockEgg("emerald", 0F);
		enderEgg = new BlockEnderEgg(); // Teleporting
		goldEgg = new BlockEgg("gold", 0F);
		ironEgg = new BlockEgg("iron", 0F);
		lapisEgg = new BlockEgg("lapis", 0F);
		obsidianEgg = new BlockEgg("obsidian", 0F);
		quartzEgg = new BlockEgg("quartz", 0F);
		redstoneEgg = new BlockRedstoneEgg(); // Lights up like ore
		spiderNest = new BlockSpiderNest();
	}
}
