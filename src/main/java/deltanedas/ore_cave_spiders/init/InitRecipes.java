package deltanedas.ore_cave_spiders.init;

import deltanedas.ore_cave_spiders.Crafting;
import deltanedas.ore_cave_spiders.Smelting;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class InitRecipes {
	public InitRecipes() {
		if (InitConfig.eggsSmeltable) {
			Smelting.add(InitBlocks.blazeEgg, 1, Items.BLAZE_ROD, 1, 5);
			Smelting.add(InitBlocks.coalEgg, 1, Items.COAL, 1, 5);
			Smelting.add(InitBlocks.diamondEgg, 1, Items.DIAMOND, 1, 5);
			Smelting.add(InitBlocks.emeraldEgg, 1, Items.EMERALD, 1, 5);
			Smelting.add(InitBlocks.enderEgg, 1, Items.ENDER_PEARL, 1, 5);
			Smelting.add(InitBlocks.goldEgg, 1, Items.GOLD_INGOT, 1, 5);
			Smelting.add(InitBlocks.ironEgg, 1, Items.IRON_INGOT, 1, 5);
			Smelting.add(new ItemStack(InitBlocks.lapisEgg, 1), new ItemStack(Items.DYE, 1, 4), 5);
			Smelting.add(InitBlocks.obsidianEgg, 1, Item.getItemFromBlock(Blocks.OBSIDIAN), 1, 5);
			Smelting.add(InitBlocks.quartzEgg, 1, Items.QUARTZ, 1, 5);
			Smelting.add(InitBlocks.redstoneEgg, 1, Items.REDSTONE, 1, 5);
		}
		Crafting.addShaped(Item.getItemFromBlock(InitBlocks.spiderNest), 8,
			"ABC",
			"DWE",
			"FGH",
			'W', new ItemStack(Blocks.WEB, 1, 0),
			'A', new ItemStack(InitBlocks.coalEgg, 1, 0),
			'B', new ItemStack(InitBlocks.diamondEgg, 1, 0),
			'C', new ItemStack(InitBlocks.emeraldEgg, 1, 0),
			'D', new ItemStack(InitBlocks.goldEgg, 1, 0),
			'E', new ItemStack(InitBlocks.ironEgg, 1, 0),
			'F', new ItemStack(InitBlocks.lapisEgg, 1, 0),
			'G', new ItemStack(InitBlocks.obsidianEgg, 1, 0),
			'H', new ItemStack(InitBlocks.redstoneEgg, 1, 0));
	}
}
