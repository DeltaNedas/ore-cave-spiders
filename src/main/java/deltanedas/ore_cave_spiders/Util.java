package deltanedas.ore_cave_spiders;

import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;

public class Util {
	public static ItemStack playerContainsItem(InventoryPlayer inventory, String name) {
		name = "item." + name;
		for (ItemStack s : inventory.mainInventory) {
			if (s != null && s.getItem().getUnlocalizedName() == name) {
				return s;
			}
		}
		return (ItemStack) null;
	}
}